/*
 * Copyright (c) 2017, Washow Corppration
 * All rights reserved.
 *
 * This code is only designed for ARDUINO NANO and SIC9311/SIC4310.
 * All timings and sequences were suitable for in specified application
 * And only use for programming reference.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "SIC9310_SPI.h"
#include "SIC9310.h"
#include "MifareUltralight.h"
#include <EEPROM.h>
#include <SPI.h>
#define DEBUG


#define RSTPD 9
#define IRQ 2
#define CS 10

#define LED_ERROR 4
#define LED_SPI 5
#define LED_TAG 6
#define LED_READ 7
#define LED_WRITE 8
#define BUZZER 3

#define MODE 5
#define TAG_MEMORY_SIZE 128
#define NDEF_MESSAGE_NUMBER 4
SIC9310_SPI sic9310spi(SPI, CS, RSTPD, IRQ);
SIC9310 nfc = SIC9310(sic9310spi);
MifareUltralight tag = MifareUltralight(nfc);

byte rxbuf[TAG_MEMORY_SIZE];
int mode_state = LOW;
int ic_type = 0;
int tag_in = 0;
uint8_t mode = 0;
long mode_time, tag_time, setting_time;
uint8_t indexMsg = 0;
int ledMap[5]={LED_SPI, LED_TAG, LED_READ, LED_WRITE, LED_ERROR};
byte ledStatus=0;

void setup(){
  Serial.begin(115200);
  Serial.println("Startup QC ver3.1");
  delay(100);
  pinMode(LED_ERROR, OUTPUT);
  pinMode(LED_SPI, OUTPUT);
  pinMode(LED_TAG, OUTPUT);
  pinMode(LED_READ, OUTPUT);
  pinMode(LED_WRITE, OUTPUT);
  pinMode(BUZZER, OUTPUT);
  ledStatus = 0b00011111;
  showLED();
  delay(1000);
  ledStatus=0b00000000;
  showLED();
  pinMode(RSTPD, OUTPUT);
  digitalWrite(RSTPD, LOW);
  alarmBeep(BUZZER);
  delay(50);
}

void loop(){  
  
  int page=0;
  byte rx[4],tx[4];

  nfc.begin();
  
#ifdef DEBUG

#endif
  ic_type = nfc.checkDEVICE();
  if(ic_type <= 0 ){
    ledStatus=0b00000000;
    showLED();    //SPI not ready or ERROR;
  }else{
    if (tag_in == 0) {
        switch(ic_type){
          case 410:
            ledStatus = 0b00001001;
            break;
          case 311:
            ledStatus = 0b00010101;
            break;
          case 310:
            ledStatus = 0b00000101  ;
            break;
          case 101:
            ledStatus = 0b00010010;
            break;  
          case 100:
            ledStatus = 0b00000010;
            break;
          default:
            ledStatus = 0b00011111;
            break;  
        }
        showLED();
    }
    nfc.config14443A();
    nfc.idle();
    if(nfc.isTagPresent()){
          ledStatus = 0b00000000;
          tag_in=1;
       //    Serial.println("present");
          ledStatus |= 0b00000010;
          if(nfc.mifareultralight_ReadPage(16, rx)==0){
             ledStatus |= 0b00000000;
          }else{
             ledStatus |= 0b00000100;        
          }
      
          for(int i=0; i < 4; i++){
            tx[i]= random(0, 256);
          }    
          if(nfc.mifareultralight_WritePage(16,tx)==1){
            ledStatus |= 0b00001000;
          }else{
            ledStatus |= 0b00000000;
          }
          showLED();
          alarmBeep(BUZZER);
          delay(200);
    }else{
       tag_in=0;
       delay(500);
       ledStatus=0b00000000;
       showLED();
       delay(500);   
    }
  }
  
}

void alarmBeep(int pin) {
  tone(pin, 5800, 500);
  //delay(2000); 
}
  
void showLED() {
  for(int i =0; i< 5; i++){
    digitalWrite(ledMap[i], (ledStatus&(1<<i))!=0?HIGH:LOW);
  }
}